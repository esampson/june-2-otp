#
#  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#

__all__ = ['AthMonitorCfgHelper']
from AthenaMonitoring.AthMonitorCfgHelper import AthMonitorCfgHelper
